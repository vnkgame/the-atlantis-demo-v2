using Nguyenkk.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nguyenkk.Framework
{
    public class Hero : Actor
    {
        [SerializeField] private GameObject effect_1;
        [SerializeField] private GameObject effect_2;
        [SerializeField] private GameObject effect_3;
        [SerializeField] private GameObject effect_4;
        [SerializeField] private GameObject effect_5;
        [SerializeField] private GameObject effect_6;

        private string eff_1 = "eff_1";
        private string eff_2 = "eff_2";
        private string eff_3 = "eff_3";
        private string eff_4 = "eff_4";
        private string eff_5 = "eff_5";
        private string eff_6 = "eff_6";

        protected override void OnEnable()
        {
            base.OnEnable();
            _actorAction.OnComplete += OnAnimComplete;
            _actorAction.OnEvent += OnEvent;
        }
        protected override void OnDisable()
        {
            base.OnDisable();
            _actorAction.OnComplete -= OnAnimComplete;
            _actorAction.OnEvent -= OnEvent;
        }

        private void OnEvent(AnimationInfo animationInfo, EventInfo eventInfo)
        {
            if (eventInfo.eventName.Equals(eff_1))
            {
                SpawnEffect(effect_1, trans.position);
            }
            else if (eventInfo.eventName.Equals(eff_2))
            {
                SpawnEffect(effect_2, trans.position);
            }
            else if (eventInfo.eventName.Equals(eff_3))
            {
                SpawnEffect(effect_3, trans.position);
            }
            else if (eventInfo.eventName.Equals(eff_4))
            {
                SpawnEffect(effect_4, trans.position);
            }
            else if (eventInfo.eventName.Equals(eff_5))
            {
                SpawnEffect(effect_5, trans.position);
            }
            else if (eventInfo.eventName.Equals(eff_6))
            {
                SpawnEffect(effect_6, trans.position);
            }
        }

        private void OnAnimComplete(AnimationInfo animationInfo)
        {
        }

        void SpawnEffect(GameObject prefab, Vector3 pos, float timeDestroy = 1.5f)
        {
            GameObject eff = Instantiate(prefab, pos, Quaternion.identity);
            Destroy(eff, timeDestroy);
        }
    }

}